defmodule RBS.Article.Uploader do
  require Logger
  alias RBS.Repo

  @articles_repo :articles
  @errors_repo :errors
  @success_repo :success
  @blog_id 22_001_221_722

  def start do
    Repo.connect_or_create(@articles_repo)
    Repo.connect_or_create(@errors_repo)
    Repo.connect_or_create(@success_repo)

    @articles_repo
    |> Repo.all()
    |> Enum.each(fn {url, %{title: title}} = article ->
      case Shopify.Blog.upload_article(@blog_id, cast(article)) do
        {:ok, _body} ->
          Logger.info("uploaded #{title}")
          Repo.insert(@success_repo, {url})
          {:ok, url}

        {:error, reason} ->
          Logger.warn(reason)
          Repo.insert(@errors_repo, {url})
          {:error, url}
      end
    end)
  end

  defp cast({"https://restonic.com/blog/" <> handle, %{title: title, image: image, body: body}}) do
    %{
      "article" => %{
        "title" => title,
        "body_html" => body,
        "handle" => handle,
        "published" => true,
        "author" => "John Doe",
        "image" => %{
          "src" => image,
          "alt" => title
        }
      }
    }
  end
end
