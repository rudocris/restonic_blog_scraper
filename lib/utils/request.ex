defmodule Utils.Request do
  require Logger

  @max_retries 50
  @timeout 5_000

  @type url :: String.t()
  @type html :: String.t()

  @spec get(url, integer) :: html
  def get(url, retries \\ @max_retries)
  def get(url, 0), do: raise("Failed to fetch from #{url}, after #{@max_retries} retries")

  def get(url, retries) when retries > 0 do
    case HTTPoison.get(url, [], follow_redirect: true, max_redirect: 5, recv_timeout: 60_000) do
      {:ok, %HTTPoison.Response{body: body, status_code: 200}} ->
        body

      {:error, reason} ->
        :timer.sleep(@timeout)
        Logger.warn("retry to get #{url}")
        get(url, retries - 1)
    end
  end
end
