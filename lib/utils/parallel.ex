defmodule Utils.Parallel do
  @spec pmap([any], (any -> [any])) :: [any]
  def pmap(collection, func) do
    collection
    |> Enum.map(&Task.async(fn -> func.(&1) end))
    |> Enum.map(&Task.await/1)
  end
end
