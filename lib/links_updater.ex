defmodule RBS.Article.Updater do
  require Logger

  @blog_id 22_001_221_722
  def start do
    @blog_id
    |> get_all_articles()
    |> update_links_and_upload()
  end

  defp update_links_and_upload(articles) do
    RBS.Repo.connect_or_create(:success_updated)

    Enum.each(articles, fn article ->
      if RBS.Repo.get(:success_updated, article["handle"]) == [] do
        article
        |> Map.update("body_html", "", &update_links/1)
        |> upload_article(article["id"])
        |> handle_upload_response()
        |> IO.inspect()
      end
    end)
  end

  defp handle_upload_response({:error, reason}), do: reason

  defp handle_upload_response(resp) do
    RBS.Repo.insert(:success_updated, {resp["article"]["handle"]})
    resp["article"]["handle"]
  end

  defp upload_article(data, article_id) do
    Shopify.Blog.update_article(@blog_id, article_id, %{
      "article" => %{
        "id" => data["id"],
        "body_html" => data["body_html"]
      }
    })
  end

  defp update_links(body) do
    body
    |> Floki.attr("a[href]", "href", &replace_if_can/1)
    |> Floki.raw_html()
  end

  defp replace_if_can("/blog" <> rest),
    do: ("." <> rest) |> IO.inspect()

  defp replace_if_can("https://restonic.com/blog" <> rest),
    do: ("." <> rest) |> IO.inspect()

  defp replace_if_can(url),
    do: url

  defp get_all_articles(blog_id, all_articles \\ [], page \\ 1) do
    case Shopify.Blog.get_article(blog_id, %{page: page}) do
      %{"articles" => []} ->
        all_articles

      %{"articles" => articles} ->
        get_all_articles(blog_id, all_articles ++ articles, page + 1)

      {:error, reason} ->
        raise reason
    end
  end
end
