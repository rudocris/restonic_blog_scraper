defmodule Shopify.Blog do
  @resource "blogs"

  @spec upload_article(integer(), {String.t(), %RBS.Article{}}) :: {:error, any()} | {:ok, any()}
  def upload_article(blog_id, article) do
    Shopify.API.request(:post, "#{@resource}/#{blog_id}/articles", article)
  end

  def update_article(blog_id, article_id, article) do
    Shopify.API.request(:put, "#{@resource}/#{blog_id}/articles/#{article_id}", article, 10)
  end

  def get_article(blog_id, params \\ %{"limit" => 250}) do
    Shopify.API.request(:get, "#{@resource}/#{blog_id}/articles", params)
  end
end
