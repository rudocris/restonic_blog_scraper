defmodule Shopify.API do
  require Logger

  @timeout 5_000

  @doc """
  Makes a request, for resource names and params docs see more at
  https://help.shopify.com/en/api/reference

  ## Parameters

    - method: Atom that represents the method (:post, :get, :put etc.)
    - resource: String that represents the Shopify resource (blogs, blogs/123/article, orders etc.)
    - params: (Optional) Map with the params to sent to the resource
    - retries: (Optional) how many times to try to make a request

  """
  def request(method, resource, params \\ [], retries \\ 1)

  def request(method, resource, _params, 0) do
    {:error, "Failed to make a #{method} request to #{resource}"}
  end

  def request(method, resource, params, retries) do
    case do_request(method, resource, params) do
      %HTTPoison.Response{status_code: 200, body: body} ->
        Jason.decode!(body)

      response ->
        :timer.sleep(@timeout)
        Logger.warn("retrying to make a request to #{resource}")
        IO.inspect(response)
        request(method, resource, params, retries - 1)
    end
  end

  defp do_request(:get, resource, params) do
    HTTPoison.get!(
      "#{url_for(resource)}?#{encode(params, :get)}",
      [
        {"Content-Type", "application/json"}
      ],
      recv_timeout: 60_000
    )
  end

  defp do_request(method, resource, params) when method in [:post, :put] do
    HTTPoison.request!(
      method,
      url_for(resource),
      encode(params, :post),
      [
        {"Content-Type", "application/json"}
      ],
      recv_timeout: 60_000
    )
  end

  defp url_for(resource) do
    {api_key, password, name} = Application.get_env(:restonic_blog_scraper, :shopify_api_keys)
    "https://#{api_key}:#{password}@#{name}.myshopify.com/admin/#{resource}.json"
  end

  defp encode(params, :post), do: Jason.encode!(params)
  defp encode(params, :get), do: URI.encode_query(params)
end
