defmodule RestonicBlogScraper do
  @moduledoc """
  Documentation for RestonicBlogScraper.
  """
  require Logger

  alias Utils.Request
  alias RBS.{Parser, Repo}

  @root_url "https://restonic.com/blog"
  @pages_to_scrape 0..0
  @repo :articles

  @parallel_opts [
    ordered: false,
    max_concurrency: 1,
    timeout: 1000 * 60 * 60
  ]

  def process do
    @pages_to_scrape
    |> Task.async_stream(
      &get_collection("#{@root_url}/page/#{&1}"),
      @parallel_opts
    )
    |> Stream.run()
  end

  defp get_collection(collection_page_url) do
    collection_page_url
    |> Request.get()
    |> Parser.collection()
    |> Task.async_stream(&get_article/1, @parallel_opts)
    |> Stream.run()
  end

  defp get_article(url) do
    {:ok, articles} = Repo.connect_or_create(@repo)

    if Repo.get(articles, url) == [] do
      url
      |> Request.get()
      |> Parser.article()
      |> (&Repo.insert(articles, {url, &1})).()
    end
  end
end
