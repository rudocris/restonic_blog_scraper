defmodule RBS.Article do
  defstruct title: nil, body: nil, image: nil
end
