defmodule RBS.Repo do
  @type url :: String.t()
  @type article :: map

  def connect_or_create(repo) do
    PersistentEts.new(repo, "#{repo}.tab", [:named_table, :public])
    {:ok, repo}
  end

  def insert(repo, record) do
    :ets.insert_new(repo, record)
  end

  def get(repo, url) do
    :ets.lookup(repo, url)
  end

  def last(repo) do
    case :ets.last(repo) do
      :"$end_of_table" -> nil
      record -> record
    end
  end

  def all(repo), do: :ets.tab2list(repo)
end
