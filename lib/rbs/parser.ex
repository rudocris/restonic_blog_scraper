defmodule RBS.Parser do
  alias RBS.Article

  @type url :: String.t()
  @type html :: String.t()

  @spec collection(html) :: [url]
  def collection(page) do
    page
    |> Floki.find(".blogpost .entry-header > a")
    |> Floki.attribute("href")
  end

  @spec article(html) :: RBS.Article.t()
  def article(page) do
    title =
      page
      |> Floki.find("h1.entry-title")
      |> Floki.text()

    image_regex = ~r/background-image: url\('(?<image>.*)'\)/

    image =
      page
      |> Floki.find(".zoomer")
      |> Floki.attribute("style")
      |> List.first()
      |> (&Regex.named_captures(image_regex, &1)).()
      |> Map.get("image")

    article_body =
      page
      |> Floki.find(".post-page-content .text-left")
      |> Floki.raw_html()

    %Article{title: title, body: article_body, image: image}
  end
end
