defmodule RBS.RepoTest do
  use ExUnit.Case

  @repo :test

  setup context do
    IO.puts("test #{context.test} start")

    case RBS.Repo.connect_or_create(@repo) do
      {:ok, _repo} -> IO.puts("conected to #{@repo} repo")
      err -> IO.inspect(err)
    end

    on_exit(fn ->
      case File.rm("./#{@repo}.tab") do
        :ok -> IO.puts("removed #{@repo} repo")
        err -> IO.inspect(err)
      end
    end)
  end

  test "insert to repo" do
    RBS.Repo.insert(@repo, {"foo", "bar"})
    assert [{"foo", "bar"}] = RBS.Repo.get(@repo, "foo")
  end
end
