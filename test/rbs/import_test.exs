defmodule RBS.ImportTest do
  use ExUnit.Case
  alias RBS.{Article, Repo, Parser}
  alias Utils.Request

  @root_url "https://restonic.com/blog/page/2"
  @article_url "https://restonic.com/blog/vanderbilt-biltmore-estate-239473"
  @articles_collection ~w(https://restonic.com/blog/warm-milk-bedtime-ritual-5389 https://restonic.com/blog/design-perfect-bedroom-scottliving-propertybrothers-128749 https://restonic.com/blog/bedtime-habits-biltmore-vanderbilt-29343 https://restonic.com/blog/7-myths-killing-your-sleep-5938 https://restonic.com/blog/famous-love-stories-sleep-6354 https://restonic.com/blog/property-brothers-2019-resolutions https://restonic.com/blog/sleep-better-adjustable-bed-3967 https://restonic.com/blog/your-ultimate-sleep-guide-for-2019-leading-trends-expert-advice-hot-new-products https://restonic.com/blog/vanderbilt-biltmore-estate-239473 https://restonic.com/blog/healthy-holiday-breakfast-recipes-3956 https://restonic.com/blog/exercise-sleep-better-45978 https://restonic.com/blog/7-ways-tweak-holiday-traditions-more-sleep-4937 https://restonic.com/blog/expert-tips-holiday-stress-20752 https://restonic.com/blog/live-longer-healthy https://restonic.com/blog/why-you-wake-up-middle-of-the-night-5737 https://restonic.com/blog/2018-sleep-horoscope-4591 https://restonic.com/blog/create-cozy-holiday-guest-room-5837 https://restonic.com/blog/go-big-with-a-california-king https://restonic.com/blog/halloween-treats-wreck-sleep-2547 https://restonic.com/blog/tips-winterproof-sleep-68716)
  @repo :articles

  test "import article from a web page" do
    @article_url
    |> Request.get()
    |> Parser.article()
    |> (&Repo.insert(@repo, {@article_url, &1})).()

    :timer.sleep(5000)

    assert [
             {@article_url,
              %Article{
                title:
                  "6 Essential Good Morning Habits From the Vanderbilt Family & Their Beloved Biltmore® Estate",
                image:
                  "https://restonic.com/wp-content/uploads/2018/07/Biltmore-blog-header-2-2.jpg",
                body: _
              }}
           ] = Repo.get(@repo, @article_url)
  end

  test "get article urls from an article collection page" do
    collection =
      @root_url
      |> (&Request.get(@repo, &1)).()
      |> Parser.collection()

    assert collection = @articles_collection
  end
end
