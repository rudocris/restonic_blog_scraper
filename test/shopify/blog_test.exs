defmodule Shopify.BlogTest do
  use ExUnit.Case

  @blog_id 22_001_221_722
  @repo :articles

  test "Upload article to shopify" do
    RBS.Repo.connect_or_create(@repo)
    [article] = RBS.Repo.get(@repo, RBS.Repo.last(@repo))
    assert {:ok, _body} = Shopify.Blog.upload_article(@blog_id, article)
  end
end
